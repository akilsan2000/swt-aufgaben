import random
from selenium import webdriver
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time
import re

# driver = webdriver.Firefox(service=FirefoxService(GeckoDriverManager().install()))
service = FirefoxService(executable_path='../drivers/geckodriver.exe')
options = webdriver.FirefoxOptions()
driver = webdriver.Firefox(service=service, options=options)

# url = "https://de.wikipedia.org/wiki/Bananen"
url = "https://de.wikipedia.org/wiki/Geschichte_der_Philosophie"
driver.get(url)


clicks = 0
n = 20

while clicks < n:
    title = driver.find_element(By.CSS_SELECTOR, "#content .mw-page-title-main").text

    if title == "Philosophie":
        print("Seite Philosophie gefunden")
        time.sleep(3)
        break

    link = driver.find_element(By.CSS_SELECTOR, "#mw-content-text p a:not(i)")
    link.click()
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "firstHeading")))

    clicks += 1

    time.sleep(1)


print("Anzahl Klicks: %d" % clicks)

driver.quit()